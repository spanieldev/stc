package spanieldev.tetris;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Effect;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.*;
import java.util.function.Consumer;

public class SimpleTetrisClone extends Application {

    public static final Color COLOR_BACKGROUND = Color.BLACK.brighter().brighter();
    public static final Color COLOR_DEADBLOCK = Color.DARKGRAY;
    public static final Color COLOR_CURRENTBLOCK = Color.RED;

    enum ACTIONS {LEFT, RIGHT, DOWN, TURN_LEFT, TURN_RIGHT}

    static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    static final int[] BLOCK_DATA = new int[]{
            1, 0, 1, 1, 1, 2, 1, 3, 0, 1, 1, 1, 2, 1, 3, 1, 1, 0, 1, 1, 1, 2, 1, 3, 0, 1, 1, 1, 2, 1, 3, 1, // ----
            1, 0, 1, 1, 1, 2, 2, 2, 0, 1, 1, 1, 2, 1, 0, 2, 1, 0, 1, 1, 1, 2, 0, 0, 0, 1, 1, 1, 2, 1, 2, 0, // --|
            1, 0, 1, 1, 1, 2, 0, 2, 0, 1, 1, 1, 2, 1, 0, 0, 1, 0, 1, 1, 1, 2, 2, 0, 0, 1, 1, 1, 2, 1, 2, 2, // --|
            1, 1, 2, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 2, 2, 2, // ||
            0, 1, 1, 1, 2, 1, 1, 0, 1, 0, 1, 1, 1, 2, 2, 1, 0, 1, 1, 1, 2, 1, 1, 2, 1, 0, 1, 1, 1, 2, 0, 1, // -|-
            0, 1, 1, 1, 2, 0, 1, 0, 1, 0, 1, 1, 2, 2, 2, 1, 0, 1, 1, 1, 2, 0, 1, 0, 1, 0, 1, 1, 2, 2, 2, 1, // --__
            0, 0, 1, 1, 2, 1, 1, 0, 2, 0, 1, 1, 1, 2, 2, 1, 0, 0, 1, 1, 2, 1, 1, 0, 2, 0, 1, 1, 1, 2, 2, 1}; // __--

    static Random random = new Random(System.currentTimeMillis());
    static GraphicsContext gc;
    static final int SPEEDRATIO = 500;
    static final int FRAMERATE = 144;
    static final long KEYRATE = 100;
    static final long KEYRATE_DOWN = 50;
    // VARS
    static final int SIZE_SCOREBOARD = 300;
    static final int BLOCK_SIZE = 30;
    static final int RATIO_GAP_BLOCK = 25;
    static final int RATIO_BLOCK_INNERBLOCK = 10;
    static final Point BOARD_OFFSET = new Point(10, 10);
    static final Point[][][] BLOCKS = new Point[7][4][4];
    static final Point BOARD_SIZE = new Point(10, 20);

    static boolean[][] board;
    static Timeline gameLoop;
    static Effect effectPause;
    // Game Config
    static int LINES_PER_LEVEL = 10;
    static long MAX_TIME = 180000;
    static int startLevel = 1;
    // Game State
    static Map<ACTIONS, Long> lastAction = new HashMap<>();
    static Map<ACTIONS, Boolean> actionsToProcess = new HashMap();
    static Map<ACTIONS, BooleanProperty> actionKeyPressed = new HashMap();
    static long startTime = 0;
    static int nextBlock = 0;
    static long score = 0;
    static long lines = 0;
    static long level = 0;
    static Point currentPos;
    static int[] currentBlock = null;
    static long lastMove = 0;
    static long lastKey = 0;
    static boolean pause = false;
    static StringBuffer log = new StringBuffer();
    static boolean gameover = false;

    static void log(String s) {
        String str = System.currentTimeMillis() - startTime + " : " + s;
        System.out.println(str);
        log.append(str + "\n");
    }


    static void pressed(ACTIONS action) {
        long rate = action == ACTIONS.DOWN ? KEYRATE_DOWN : KEYRATE;
        long t = System.currentTimeMillis();
        if (t - lastAction.get(action) > rate) {
            actionsToProcess.put(action, true);
            lastAction.put(action, System.currentTimeMillis());
            log(" ACTION : " + action);
        }
    }

    static void released(ACTIONS k) {
        actionsToProcess.put(k, false);
    }

    public static void init(int... block_data) {
        initBoxBlur();
        board = new boolean[(int) BOARD_SIZE.x][(int) BOARD_SIZE.y];
        // read block data
        for (int p = 0; p < block_data.length / 2; p++) {
            BLOCKS[(p / 16) % 16][(p / 4) % 4][p % 4] = new Point(block_data[p * 2], block_data[p * 2 + 1]);
        }
        for (ACTIONS k : ACTIONS.values()) {
            actionsToProcess.put(k, false);
            lastAction.put(k, 0L);
            actionKeyPressed.put(k, new SimpleBooleanProperty(false));
        }
        level = startLevel;
        startTime = System.currentTimeMillis();
        nextBlock = random.nextInt(7);
    }

    public static void drawBackground() {
        gc.setFill(Color.DARKGRAY);
        gc.fillRect(0, 0, BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x * 2 + SIZE_SCOREBOARD, BOARD_SIZE.y * BLOCK_SIZE + BOARD_OFFSET.y * 2);
        gc.setFill(Color.BLACK);
        gc.fillRect(BOARD_OFFSET.x, BOARD_OFFSET.y, BOARD_SIZE.x * BLOCK_SIZE, BOARD_SIZE.y * BLOCK_SIZE);
    }

    public static void drawBoard() {
        drawSingleBlocks();
        gc.setFill(Color.LIGHTGRAY);
        gc.setFont(Font.font(30));
        gc.fillRect(BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x, BOARD_OFFSET.y, SIZE_SCOREBOARD, BOARD_SIZE.y * BLOCK_SIZE);
        gc.setFill(Color.BLACK);
        gc.fillText("Lines: " + Long.toString(lines), BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x + 30, BOARD_OFFSET.y + 50);
        gc.fillText("Score: " + Long.toString(score), BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x + 30, BOARD_OFFSET.y + 90);
        gc.fillText("Level: " + Long.toString(level), BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x + 30, BOARD_OFFSET.y + 130);
        gc.fillText("Timer: " + Long.toString((MAX_TIME - (System.currentTimeMillis() - startTime)) / 1000), BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x + 30, BOARD_OFFSET.y + 170);
    }

    private static void drawSingleBlocks() {
        for (int x = 0; x < BOARD_SIZE.x; x++)
            for (int y = 0; y < BOARD_SIZE.y; y++)
                if (board[x][y] == true)
                    drawSingleBlock(new Point(0, 0), new Point(x, y), COLOR_DEADBLOCK);
                else
                    drawSingleBlock(new Point(0, 0), new Point(x, y), COLOR_BACKGROUND);
    }

    public static void drawBlock(int block, int var, Point pos, Color fill) {
        for (Point p : BLOCKS[block][var]) drawSingleBlock(p, pos, fill);
    }

    public static void drawSingleBlock(Point block, Point pos, Color fill) {
        gc.setFill(fill);
        gc.fillRect(BOARD_OFFSET.x + pos.x * BLOCK_SIZE + block.x * BLOCK_SIZE + BLOCK_SIZE / RATIO_GAP_BLOCK, BOARD_OFFSET.y + pos.y * BLOCK_SIZE + block.y * BLOCK_SIZE + BLOCK_SIZE / RATIO_GAP_BLOCK, BLOCK_SIZE - BLOCK_SIZE / RATIO_GAP_BLOCK * 2, BLOCK_SIZE - BLOCK_SIZE / RATIO_GAP_BLOCK * 2);
        gc.setFill(fill.darker());
        gc.fillRect(BOARD_OFFSET.x + pos.x * BLOCK_SIZE + block.x * BLOCK_SIZE + BLOCK_SIZE / RATIO_GAP_BLOCK + BLOCK_SIZE / RATIO_BLOCK_INNERBLOCK, BOARD_OFFSET.y + pos.y * BLOCK_SIZE + block.y * BLOCK_SIZE + BLOCK_SIZE / RATIO_GAP_BLOCK + BLOCK_SIZE / RATIO_BLOCK_INNERBLOCK, BLOCK_SIZE - BLOCK_SIZE / RATIO_GAP_BLOCK * 2 - BLOCK_SIZE / RATIO_BLOCK_INNERBLOCK * 2, BLOCK_SIZE - BLOCK_SIZE / RATIO_GAP_BLOCK * 2 - BLOCK_SIZE / RATIO_BLOCK_INNERBLOCK * 2);
    }

    static void draw() {
        drawBackground();
        drawBoard();
        drawCurrent();
        drawBlock(nextBlock, 0, new Point(12, 7), COLOR_CURRENTBLOCK);
    }

    private static void drawCurrent() {
        drawBlock(currentBlock[0], currentBlock[1], currentPos, COLOR_CURRENTBLOCK);
    }

    static boolean fits(Point newpos) {
        Point[] ps = BLOCKS[currentBlock[0]][currentBlock[1]];
        for (Point p : ps) {
            int x = (int) p.x + newpos.x;
            int y = (int) p.y + newpos.y;
            if (x < 0 || x >= BOARD_SIZE.x || y < 0 || y >= BOARD_SIZE.y || board[x][y]) return false;
        }
        return true;
    }

    static void setTurn(int p) {
        currentBlock[1] = p;
    }

    static void turn(int dir) {
        int oldp = currentBlock[1];
        int newp = (oldp + dir);
        newp = newp > 3 ? 0 : newp;
        newp = newp < 0 ? 3 : newp;
        currentBlock[1] = newp;
        if (fits(currentPos) == false) currentBlock[1] = oldp;
    }

    static void settleBlock() {
        Point[] ps = BLOCKS[currentBlock[0]][currentBlock[1]];
        for (Point p : ps) board[(int) p.x + currentPos.x][(int) p.y + currentPos.y] = true;
        clearLines(checkLines());
        nextBlock();
    }

    static void nextBlock() {
        currentPos = new Point(BOARD_SIZE.x / 2 - 2, 0);
        currentBlock = new int[]{nextBlock, 0};
        log(" ------ NEXT: " + nextBlock);
        nextBlock = random.nextInt(7);
        if (fits(currentPos) == false) {
            log(" ------ GAME OVER: " + score);
            gameover();
        }
    }

    @Override
    public void start(Stage pvPrimaryStage) throws Exception {
        // Config Dialog
        Stage dialog = new Stage();
        dialog.setWidth(200);
        dialog.setHeight(140);
        ComboBox comboBox = new ComboBox(FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
                17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30));
        Button button = new Button("Start");
        button.setOnAction(e -> closeConfigDialog(dialog, comboBox));
        comboBox.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER)
                closeConfigDialog(dialog, comboBox);
        });
        VBox box = new VBox(new Label("Level:"), comboBox, button);
        box.setSpacing(10);
        box.setAlignment(Pos.CENTER);
        Scene s = new Scene(box);
        dialog.setScene(s);
        comboBox.getSelectionModel().select(0);
        dialog.showAndWait();

        // MainScene
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, BOARD_SIZE.x * BLOCK_SIZE +
                BOARD_OFFSET.x * 2 + SIZE_SCOREBOARD,
                BOARD_SIZE.y * BLOCK_SIZE + BOARD_OFFSET.y * 2);
        pvPrimaryStage.setScene(scene);
        pvPrimaryStage.show();
// KeyMaps
        Map<KeyCode, Consumer> kmapPressed = new HashMap<>();
        kmapPressed.put(KeyCode.NUMPAD1, e -> setPos(new Point(-1, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD2, e -> setPos(new Point(0, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD3, e -> setPos(new Point(1, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD4, e -> setPos(new Point(2, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD5, e -> setPos(new Point(3, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD6, e -> setPos(new Point(4, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD7, e -> setPos(new Point(5, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD8, e -> setPos(new Point(6, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD9, e -> setPos(new Point(7, currentPos.y)));
        kmapPressed.put(KeyCode.NUMPAD0, e -> setPos(new Point(8, currentPos.y)));

        kmapPressed.put(KeyCode.DIGIT1, e -> setTurn(0));
        kmapPressed.put(KeyCode.DIGIT2, e -> setTurn(1));
        kmapPressed.put(KeyCode.DIGIT3, e -> setTurn(2));
        kmapPressed.put(KeyCode.DIGIT4, e -> setTurn(3));
        kmapPressed.put(KeyCode.LEFT, e -> actionKeyPressed.get(ACTIONS.LEFT).setValue(true));
        kmapPressed.put(KeyCode.RIGHT, e -> actionKeyPressed.get(ACTIONS.RIGHT).setValue(true));
        kmapPressed.put(KeyCode.A, e -> turn(+1));
        kmapPressed.put(KeyCode.UP, e -> turn(+1));
        kmapPressed.put(KeyCode.Q, e -> turn(-1));
        kmapPressed.put(KeyCode.DOWN, e -> actionKeyPressed.get(ACTIONS.DOWN).setValue(true));
        kmapPressed.put(KeyCode.X, e -> System.exit(0));
        Map<KeyCode, Consumer> kmapReleased = new HashMap<>();
        kmapReleased.put(KeyCode.LEFT, e -> actionKeyPressed.get(ACTIONS.LEFT).setValue(false));
        kmapReleased.put(KeyCode.RIGHT, e -> actionKeyPressed.get(ACTIONS.RIGHT).setValue(false));
        kmapReleased.put(KeyCode.L, e -> {
            pause();
            showLog();
        });

        kmapReleased.put(KeyCode.DOWN, e -> actionKeyPressed.get(ACTIONS.DOWN).setValue(false));
        kmapReleased.put(KeyCode.UP, e -> actionKeyPressed.get(ACTIONS.TURN_LEFT).setValue(false));
        kmapReleased.put(KeyCode.Q, e -> actionKeyPressed.get(ACTIONS.TURN_LEFT).setValue(false));
        kmapReleased.put(KeyCode.A, e -> actionKeyPressed.get(ACTIONS.TURN_RIGHT).setValue(false));
        scene.setOnKeyPressed(e -> {
            if (!pause) {
                log(" KEY: " + e.getCode());
                Consumer c = kmapPressed.get(e.getCode());
                if (c != null)
                    c.accept(null);
            }
            if (e.getCode() == KeyCode.SPACE)
                pause();
        });
        scene.setOnKeyReleased(e -> {
            if (!pause) {
                Consumer c = kmapReleased.get(e.getCode());
                if (c != null)
                    c.accept(null);
            }
        });
        init(BLOCK_DATA);
        nextBlock();
// CANCAS for Painting

        Canvas canvas = new Canvas(BOARD_SIZE.x * BLOCK_SIZE + BOARD_OFFSET.x
                * 2 + SIZE_SCOREBOARD,
                BOARD_SIZE.y * BLOCK_SIZE + BOARD_OFFSET.y * 2);
        gc = canvas.getGraphicsContext2D();
        root.setCenter(canvas);
        BorderPane.setAlignment(canvas, Pos.CENTER_LEFT);
// Gameloop
        final javafx.util.Duration d = javafx.util.Duration.millis(1000 / FRAMERATE);
        final KeyFrame oneFrame = new KeyFrame(d, (e) -> {
            if (!pause) {
                draw();
                if (System.currentTimeMillis() - lastMove > 10 + (SPEEDRATIO /
                        level)) {
                    lastMove = System.currentTimeMillis();
                    Point newpos = new Point(currentPos.x, currentPos.y + 1);

                    if (fits(newpos)) {
                        setPos(newpos);
                    } else {
                        settleBlock();
                    }
                    if (System.currentTimeMillis() - startTime > MAX_TIME)
                        gameover();
                }
                for (ACTIONS k : ACTIONS.values()) {
                    if (actionKeyPressed.get(k).get())
                        pressed(k);
                }
                if (actionsToProcess.get(ACTIONS.LEFT)) {
                    released(ACTIONS.LEFT);
                    setPos(new Point(currentPos.x - 1, currentPos.y));
                }
                if (actionsToProcess.get(ACTIONS.RIGHT)) {
                    released(ACTIONS.RIGHT);
                    setPos(new Point(currentPos.x + 1, currentPos.y));
                }
                if (actionsToProcess.get(ACTIONS.DOWN)) {
                    released(ACTIONS.DOWN);
                    Point np = new Point(currentPos.x, currentPos.y + 1);
                    if (fits(np)) {
                        setPos(np);
                        lastMove = System.currentTimeMillis();
                        score = score + level * 5;
                    } else {
                        settleBlock();
                    }
                }
            }
        });
        gameLoop = new Timeline(FRAMERATE, oneFrame);
        gameLoop.setCycleCount(Animation.INDEFINITE);
        gameLoop.playFromStart();
    }

    private void closeConfigDialog(Stage dialog, ComboBox comboBox) {
        startLevel = (Integer) comboBox.getSelectionModel().getSelectedItem();
        dialog.close();
    }

    private static void gameover() {
        showFinish();
        gameLoop.stop();
    }

    private static void showLog() {
        Stage stage = new Stage();
        stage.setTitle("Logs or it didn't happen!!");
        stage.setWidth(400);
        stage.setHeight(300);
        TextArea text = new TextArea();
        stage.setScene(new Scene(new HBox(text)));
        text.setText(log.toString());
        text.selectAll();
        ClipboardContent clipboardContent = new ClipboardContent();

        clipboardContent.putString(text.getText());
        Clipboard.getSystemClipboard().setContent(clipboardContent);
        stage.show();
        stage.setOnCloseRequest(e -> {
            if (pause)
                pause();
        });
    }

    private static void initBoxBlur() {
        BoxBlur bb = new BoxBlur(50, 50, 1);
        effectPause = bb;
    }

    private static void showFinish() {
        gc.setEffect(effectPause);
        draw();
        gc.setEffect(null);
        gc.setFill(Color.BLUE);
        gc.setFont(Font.font(50));
        gc.fillText("GAME OVER", 100, 200);
        gc.fillText("SCORE: " + score, 100, 250);
        gc.fillText("LINES: " + lines, 100, 300);

    }

    static void clearLines(List<Integer> pvCheckLines) {
        for (int l : pvCheckLines)
            for (int y = l; y > 0; y--)
                for (int x = 0; x < BOARD_SIZE.x; x++)
                    board[x][y] = board[x][y - 1];
        lines = lines + pvCheckLines.size();
        switch (pvCheckLines.size()) {
            case 1:
                score += 1 * (level * 10);
            case 2:
                score += 10 * (level * 10);
            case 3:
                score += 50 * (level * 10);
            case 4:
                score += 500 * (level * 10);
            default:
        }
        level = startLevel + lines / LINES_PER_LEVEL;
        if (pvCheckLines.size() > 0) {
            log(" ------ LINES: " + pvCheckLines);
            log(" ------ SCORE: " + score);
        }
    }

    static void pause() {
        pause = !pause;
        if (pause) {
            gc.setEffect(effectPause);
        } else {
            gc.setEffect(null);
        }
        draw();
    }

    static List<Integer> checkLines() {
        List<Integer> lvLines = new LinkedList<>();
        for (int y = 0; y < BOARD_SIZE.y; y++) {
            boolean fullLine = true;
            for (int x = 0; x < BOARD_SIZE.x; x++)
                if (board[x][y] == false)
                    fullLine = false;
            if (fullLine)
                lvLines.add(y);
        }
        return lvLines;
    }

    static void setPos(Point newpos) {
        if (fits(newpos))
            currentPos = newpos;
    }

    public static void main(String[] args) {
        SimpleTetrisClone.launch(args);
    }

}
